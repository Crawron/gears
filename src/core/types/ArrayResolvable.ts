export type ArrayResolvable<T> = T | T[]
