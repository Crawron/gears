export type PromiseResolvable<T> = Promise<T> | T
